#pragma once
#include <iostream>
#include <string>

using namespace std;

class NPC
{
protected:
	int nid;
	string name;
	string title;
	int maxhp;
	int maxmp;

public:
	int getnID() { return nid; }
	string getName() { return name; }
	string getTitle() { return title; }
	NPC() {};
};

class BasicMob : public NPC
{
private:
	enum Level { BANDIT, COMMANDER }; //levels of being a bad person

	Level badGuy; //the bad guys evil level

public:
	int attack();

	string getName() { return name; }

	BasicMob(int level = 1);
};