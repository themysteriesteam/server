#pragma once
#include "../MainServer/all.h"

class Player
{
private:
	bool _forceNoSpawnProtection = false;
	int _lockdownTime = 0;
	int _Race;
	int _Class;
	int _Access;
	int _X;
	int _Y;
	int _Z;
	int _Horns;
	int _Tattoo;
	int _LipsShape;
	int _LipsColor;
	int _EyebrowColor;
	int _EyebrowShape;
	int _EyeColor;
	int _EyeShape;
	int _SkinColor;
	int _faceshape;
	int _hairtype;
	int _hair;
	int _Color;
	int _BodyShape;
	int _Height;
	int _Muscles;
	int _Weight;
	int _pid;
	string _name;
	string _title;
	int _maxhp;
	int _maxmp;
	int _level;
	int _wings;
	int _currentxp;
	int _currenthp;
	int _currentmp;
	int _maxstamina;
	int _currentstamina;
	int _attackspeed;
	int _castingspeed;
	int _physicaldamage;
	int _magicaldamage;
	string _accountName;
	int _physicaldef;
	int _magicaldef;
	bool isOnline = false;
	int _deaths;
	int _pvps;
public:
	void setDeaths(int Deaths) { _deaths = Deaths; }
	void setPvPKills(int PvPs) { _pvps = PvPs; }
	int getDeaths() { return _deaths; }
	int getPvPs() { return _pvps; }
	void setOnline() { isOnline = true; }
	void setOffline() { isOnline = false; }
	void setClass(int Class) { _Class = Class; }
	void setAccess(int Access) { _Access = Access; }
	void setX(int X) { _X = X; }
	void setY(int Y) { _Y = Y; }
	void setZ(int Z) { _Z = Z; }
	void setLocation(int X, int Y, int Z) { _X = X; _Y = Y; _Z = Z; }
	void setHorns(int Horns) { _Horns = Horns; }
	void setTattoo(int Tattoo) { _Tattoo = Tattoo; }
	void setLipsShape(int LipsShape) { _LipsShape = LipsShape; }
	void setLipsColor(int LipsColor) { _LipsColor = LipsColor; }
	void setEyebrowColor(int EyebrowColor) { _EyebrowColor = EyebrowColor; }
	void setEyeBrowShape(int EyeBrowShape) { _EyebrowShape = EyeBrowShape; }
	void setEyeColor(int EyeColor) { _EyeColor = EyeColor; }
	void setEyeShape(int EyeShape) { _EyeShape = EyeShape; }
	void setSkinColor(int SkinColor) { _SkinColor = SkinColor; }
	void setFaceShape(int FaceShape) { _faceshape = FaceShape; }
	void setHairType(int HairType) { _hairtype = HairType; }
	void setHair(int Hair) { _hair = Hair; }
	void setColor(int Color) { _Color = Color; }
	void setBodyShape(int BodyShape) { _BodyShape = BodyShape; }
	void setHeight(int Height) { _Height = Height; }
	void setWeight(int Weight) { _Weight = Weight; }
	void setMuscles(int Muscles) { _Muscles = Muscles; }
	void setPID(int PID) { _pid = PID; }
	void setName(string Name) { _name = Name; }
	void setTitle(string Title) { _title = Title; }
	void setMaxHP(int MaxHP) { _maxhp = MaxHP; }
	void setMaxMP(int MaxMP) { _maxmp = MaxMP; }
	void setLevel(int Level) { _level = Level; }
	void AddXP(int XP) { _currentxp = _currentxp + XP; }
	void SetCurrentXP(int XP) { _currentxp = XP; }
	void SetMaxStamina(int Stamina) { _maxstamina = Stamina; }
	void setWings(int Wings) { _wings = Wings; }
	void setCurrentHP(int CurrentHP) { _currenthp = CurrentHP; }
	void setCurrentMP(int CurrentMP) { _currentmp = CurrentMP; }
	void setCurrentStamina(int Stamina) { _currentstamina = Stamina; }
	void setAttackSpeed(int AttackSpeed) { _attackspeed = AttackSpeed; }
	void setCastingSpeed(int CastingSpeed) { _castingspeed = CastingSpeed; }
	void setPhysicalDamage(int PhysicalDamage) { _physicaldamage = PhysicalDamage; }
	void setMagicDamage(int MagicDamage) { _magicaldamage = MagicDamage; }
	string getAccountName() { return _accountName; }
	int getPhysicalDef() { return _physicaldef; }
	int getMagicalDef() { return _magicaldef; }
	void setPhysicalDef(int PhysicalDef) { _physicaldef = PhysicalDef; }
	void setMagicalDef(int MagicalDef) { _magicaldef = MagicalDef; }
	const int NEWBIE_LEVEL = 10;
	const int REQUEST_TIMEOUT = 15;
	bool canSendUserInfo = false;
	int getLockdownTime() { return _lockdownTime; }
	void setLockdownTime(int lockdownTime) { _lockdownTime = lockdownTime; }
	int _secretCode = NULL;
	bool isForceNoSpawnProtection() { return _forceNoSpawnProtection; }
	void setForceNoSpawnProtection(bool forceNoSpawnProtection) { _forceNoSpawnProtection = forceNoSpawnProtection; }
	long clastlogin();
	bool isNewbie() { if (_level <= NEWBIE_LEVEL) return true; else return false; }
	void setRace(int Race) { _Race = Race; }
	int getWeight() { return _Weight; }
	int getHeight() { return _Height; }
	int getWings() { return _wings; }
	int getRace() { return _Race; }
	int getClass() { return _Class; }
	int getX() { return _X; }
	int getY() { return _Y; }
	int getZ() { return _Z; }
	int getAccess() { return _Access; }
	int getpID() { return _pid; }
	string getName() { return _name; }
	string getTitle() { return _title; }
	int getMaxHP() { return _maxhp; }
	int getMaxMP() { return _maxmp; }
	int getLevel() { return _level; }
	int getCurrentXP() { return _currentxp; }
	int getCurrentHP() { return _currenthp; }
	int getCurrentMP() { return _currentmp; }
	int getMaxStamina() { return _maxstamina; }
	int getCurrentStamina() { return _currentstamina; }
	int getatttackspeed() { return _attackspeed; }
	int getcastingspeed() { return _castingspeed; }
	int getphysicaldamage() { return _physicaldamage; }
	int getmagicaldamage() { return _magicaldamage; }
	void setPerson(); //gets the person's name and gender
	enum WeaponLevel { WOOD = 1, BRONZE, SILVER, STEEL };
	WeaponLevel Weapon;
	int PhysicalAttack();
	int MagicalAttack();
	Player();

};
