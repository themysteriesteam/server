#include "CharacterCreate.h"

using namespace std;
inventory i;
SQLs s;
Player NChar;

int RaceSpawnX = 1;
int RaceSpawnY = 1;
int RaceSpawnZ = 1;
int RaceChosen = 0;
string NameChosen = " ";
string PasswordChosen = " ";
int HairChosen = 0;
int ColorChosen = 0;
int WingsChosen = 0;
int ChosenHairType = 0;
int ChosenFaceShape = 0;
int ChosenSkinColor = 0;
int ChosenHorns = 0;
int ChosenEyeShape = 0;
int ChosenEyeColor = 0;
int ChosenEyebrowShape = 0;
int ChosenEyebrowColor = 0;
int ChosenLipsShape = 0;
int ChosenLipsColor = 0;
int ChosenTattoo = 0;
int BodyShapeChosen = 0;
int HeightChosen = 0;
int WeightChosen = 0;
int MusclesChosen = 0;

int GeneratePlayerID(int PlID)
{
	int size = 10;
	/*const std::string generator = "0123456789";
	for (size_t i = size; i; --i) {
	PlID.push_back(generator[rand() % generator.size;
	}*/
	return PlID;
}

void Player::setPerson()
{
	bool usedid = true;
	i.fillinv();
	/*
	while (usedid = true)
	{
	NChar.id = GeneratePlayerID();
	if (mysql_query(conn, ""SELECT * FROM characters WHERE id = '" & _
	Request.Form("username") & "NChar.id"");"))
	{
	usedid = true;
	}
	else { usedid = false; }
	}*/

	NChar.setRace(RaceChosen);

	if (RaceChosen == 3 || RaceChosen == 5)
	{
		NChar.setWings(WingsChosen);
	}
	else { NChar.setWings(0); }

	switch (RaceChosen) {
	case 1: //Human
		RaceSpawnX = 1;
		RaceSpawnY = 1;
		RaceSpawnZ = 1;
		NChar.setClass(1); //Human Base Class
		NChar.setBodyShape(BodyShapeChosen);
		NChar.setHeight(HeightChosen);
		NChar.setWeight(WeightChosen);
		NChar.setMuscles(MusclesChosen);
		break;
	case '2': //Infernal
		RaceSpawnX = 1;
		RaceSpawnY = 1;
		RaceSpawnZ = 1;
		NChar.setClass(2); //Infernal Base Class
		NChar.setBodyShape(BodyShapeChosen);
		NChar.setHeight(HeightChosen);
		NChar.setWeight(WeightChosen);
		NChar.setMuscles(MusclesChosen);
		break;
	case '3': //Dragon
		RaceSpawnX = 1;
		RaceSpawnY = 1;
		RaceSpawnZ = 1;
		NChar.setClass(3); //Dragon Base Class
		NChar.setBodyShape(BodyShapeChosen);
		NChar.setHeight(HeightChosen);
		NChar.setWeight(WeightChosen);
		NChar.setMuscles(MusclesChosen);
		break;
	case '4': //Dryad
		RaceSpawnX = 1;
		RaceSpawnY = 1;
		RaceSpawnZ = 1;
		NChar.setClass(4); //Dryad Base Class
		NChar.setBodyShape(BodyShapeChosen);
		NChar.setHeight(HeightChosen);
		NChar.setWeight(WeightChosen);
		NChar.setMuscles(MusclesChosen);
		break;
	case '5': //Ethereal
		RaceSpawnX = 1;
		RaceSpawnY = 1;
		RaceSpawnZ = 1;
		NChar.setClass(5); //Ethereal Base Class
		NChar.setBodyShape(BodyShapeChosen);
		NChar.setHeight(HeightChosen);
		NChar.setWeight(WeightChosen);
		NChar.setMuscles(MusclesChosen);
		break;
	default:
		break;
	}

	if (NChar.getAccess = 100) //GM
	{
		NChar.setX(1);
		NChar.setY(1);
		NChar.setZ(1);
	}
	else {
		NChar.setX(RaceSpawnX);
		NChar.setY(RaceSpawnY);
		NChar.setZ(RaceSpawnZ);
	}

	if (RaceChosen == 2 || RaceChosen == 3)
	{
		NChar.setHorns(ChosenHorns);
	}
	else { NChar.setHorns(0); }

	NChar.setTattoo(ChosenTattoo);
	NChar.setLipsShape(ChosenLipsShape);
	NChar.setLipsColor(ChosenLipsColor);
	NChar.setEyebrowColor(ChosenEyebrowColor);
	NChar.setEyeBrowShape(ChosenEyebrowShape);
	NChar.setEyeColor(ChosenEyeColor);
	NChar.setEyeShape(ChosenEyeShape);
	NChar.setSkinColor(ChosenSkinColor);
	NChar.setFaceShape(ChosenFaceShape);
	NChar.setHairType(ChosenHairType);
	NChar.setHair(HairChosen);
	NChar.setColor(ColorChosen);
	NChar.setName(NameChosen);
	NChar.setTitle("");
	s.INSERT_CHARACTER;
}