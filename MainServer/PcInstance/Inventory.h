#pragma 
#include "PcInstance.h"

class inventory : public Player
{
protected:
	int slots = 50;
	int usedslots;
	int freeslots = slots - usedslots;
	int inv[600];
public:
	int getSlots() { return slots; }
	int getFreeSlots() { return freeslots; }
	int getUsedSlots() { return usedslots; }
	void fillinv();
	void additem();
	void deleteitem();
	void deleteinventory();
	void swapslots();
	inventory(void) {}
	~inventory(void) {}
};