#pragma once
#include "../mmo/server.h"

class RequestServerLogin
{
private:
	int _skey1;
	int _skey2;
	int _serverId;
public:
	int getSessionKey1() { return _skey1; }
	int getSessionKey2() { return _skey2; }
	int getServerId() { return _serverId; }
	bool readImpl()
	{
		_skey1 = RecieveInt();
		_skey2 = RecieveInt();
		_serverId = RecieveInt();
		return true;
	}
	void Run()
	{

	}
};