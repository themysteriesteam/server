#pragma once
#include "../mmo/server.h"
#include <vector>
#include <string>

using namespace std;

class PlayerInGame
{
private:
	vector<string> _accounts;
public:
	PlayerInGame()
	{
		_accounts = vector<string>();
		int size = RecieveInt();
		for (int i = 0; i < size; i++)
		{
			_accounts.push_back(RecieveString());
		}
	}
	vector<string> getAccounts() { return _accounts; }
};