#pragma once
#include "../mmo/server.h"

class LoginServerFail
{
public:
	LoginServerFail(int reason)
	{
		SendInt(reason);
	}
	int REASON_IP_BANNED = 1;
	int REASON_IP_RESERVED = 2;
	int REASON_ALREADY_LOGGEDIN = 3;
};