#pragma once
#include <vector>
#include <string>
#include "../mmo/server.h"

using namespace std;

class AbnormalStatusUpdate
{
private:
	string ABNORMALSTATUESUPDATE = "Abnormal statues update";
	Effect e;
	vector<Effect> _effects;
	
	AbnormalStatusUpdate()
	{
		_effects = vector<Effect>();
	}
	void addEffect(int skillId, int level, int duration)
	{
		_effects.PUSH_BACK(new Effect(skillId, level, duration));
	}
	void WriteImpl()
	{
		SendInt(_effects.size());
		for (Effect temp : _effects)
		{
			SendInt(temp._skillid);
			SendInt(temp._level);

			if (temp._duration == -1)
				SendInt(-1);
			else
				SendInt(temp._duration / 1000);
		}
	}
};
class Effect
{
protected:
	int _skillid;
	int _level;
	int _duration;
public:
	Effect(int pskillid, int plevel, int pduration)
	{
		_skillid = pskillid;
		_level = plevel;
		_duration = pduration;
	}
};