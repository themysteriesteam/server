#pragma once
#include "../mmo/server.h"

class LoginFail
{
public:
	static enum LoginFailReason
	{
		REASON_SYSTEM_ERROR,
		REASON_WRONG_PASS,
		REASON_USER_OR_PASS_WRONG,
		REASON_ACCESS_FAILED,
		REASON_ACCOUNT_IN_USE,
		REASON_SERVER_OVERLOADED,
		REASON_SERVER_MAINTENANCE,
		REASON_TEMP_PASS_EXPIRED,
		REASON_DUAL_BOX
	};
	LoginFailReason _reason;
	LoginFail(LoginFailReason reason)
	{
		_reason = reason;
	}
	int getReason() { return _reason; }
protected:
	void write()
	{
		SendInt(getReason());
	}
};