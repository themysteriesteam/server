#pragma once
#include "../mmo/server.h"
#include <string>

using namespace std;

class ChangeAccessLevel
{
private:
	int _level;
	string _account;
public:
	ChangeAccessLevel()
	{
		_level = RecieveInt();
		_account = RecieveString();
	}
	string getAccount() { return _account; }
	int getLevel() { return _level; }
};