#pragma once
#include "../mmo/server.h"
#include "string.h"

using namespace std;

class PlayerAuthResponse
{
public:
	PlayerAuthResponse(string account, bool response)
	{
		SendString(account);
		SendBool(response);
	}
};