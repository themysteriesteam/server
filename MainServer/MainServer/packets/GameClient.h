#pragma once
#include "../mmo/server.h"
#include <string>
#include <vector>
#include "../crypt/main.h"
#include "../mmo/time.h"
#include "../mysql/SQLStrings.h"

using namespace std;
SQLs sqlStrings;

class GameClient {
private:
	string _accountName;
	string _password;
	bool _loggingOut = false;
	bool _married = false;
	bool _isAuthedGG;
	long _connectionStartTime;
	vector<int> _charSlotMaping;
	ENC _crypt;
	bool _isDetached = false;
	bool _protocol;
	int getObjectIdForSlot(int charslot)
	{
		int maxslots = 5; // shoud change
		if (charslot < 0 || charslot >= maxslots)
		{
			cout << "Tried to use char slot smaller or bigger than the allowed slots!" << endl;
		}
		int objectId = charslot;
		return objectId.intValue();
	}
public:
	enum GameClientState { CONNECTED, AUTHED, IN_GAME };
	GameClientState state;
	bool isLogginOut() { return _loggingOut; }
	void setLoggingOut(bool loggingOut) { _loggingOut = loggingOut; }
	string getPassword() { return _password; }
	void setPassword(string password) { _password = password; }
	bool isTheAccountMarried() { return _married; }
	bool setAccountMarried(bool m) { _married = m; }
	GameClient()
	{
		state = GameClientState::CONNECTED;
		_connectionStartTime = getCurrentMillis();
		// auto save db
	}
	void loadMarriegeStatus()
	{
		sqlStrings.CheckAccMarriege;
		sqlStrings.setString(1, _accountName);
		if ("married" > 0)
		{
			_married = true;
		}
		else
		{
		}
	}
	GameClientState getState() { return state; }
	void setState(GameClientState pstate) { state = pstate; }
	long getConnectionStartTime() { return _connectionStartTime; }
	//PcInstance getActiveChar() { return _activeChar;}
	string setAccountName(string accname) { _accountName = accname; }
	string getAccountName() { return _accountName; }
	bool isDetached() { return _isDetached; }
	bool setDetached(bool d) { _isDetached = d; }
	void markDeleteChar(int charslot)
	{
		int objid = getObjectIdForSlot(charslot);
		if (objid < 0)
			return -1;
		/*

		delete from my sql char

		*/
	}
	void SaveCharToDisk(/* PcInstance cha*/)
	{
		//save into db{}
	}
	void loadCharFromDisk(int charslot)
	{
		//store char from db
	}
	void setCharSelection(int charslot)
	{
		SendInt(charslot);
	}

};