#include "server.h"

int StartServer(char *IP, int Port)
{
	int err;
	WSAStartup(MAKEWORD(2, 2), &Data);
	sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	//Protocols: (IPPROTO_UDP[UDP Port], IPPROTO_TCP[TCP Port], IPPROTO_RM[Multicast type], BTHPROTO_RFCOMM[Bluetooth Radio Frequency Communications])
	if (sock == INVALID_SOCKET)
	{
		cout << "Invalid Socket" << endl;
		cin.get();
		exit(0);
		return 0;
	}
	i_sock.sin_family = AF_INET;
	i_sock.sin_addr.s_addr = inet_addr(IP);
	i_sock.sin_port = htons(Port);
	err = bind(sock, (LPSOCKADDR)&i_sock, sizeof(i_sock));
	if (err != 0)
	{
		return 0;
	}
	err = listen(sock, maxclients);
	if (err == SOCKET_ERROR)
	{
		return 0;
	}
	while (1)
	{
		for (int i = 0; i < maxclients; i++)
		{
			if (clients < maxclients)
			{
				int so2len = sizeof(i_sock2);
				sock2[clients] = accept(sock, (sockaddr *)&i_sock2, &so2len);
				if (sock2[clients] == INVALID_SOCKET)
				{
					return 0;
				}
				cout << "A client has joined the server with IP: " << i_sock2.sin_addr.s_addr << endl;
				// example for variables inside " " printf("a client has joined the server(IP: %s)\\n", i_sock2.sin_addr.s_addr);
				clients++;
				continue;
			}
			else
			{
				break;
			}
		}
	}
	return 1;
}

int Send(char *Buf, int len, int Client)
{
	int slen = send(sock2[Client], Buf, len, 0);
	if (slen < 0)
	{
		printf("Cannot send data!");
		cin.get();
		return 1;
	}
	return slen;
}

int Recieve(char *Buf, int len, int Client)
{
	int slen = recv(sock2[Client], Buf, len, 0);
	if (slen < 0)
	{
		printf("Cannot send data !");
		cin.get();
		return 1;
	}
	return slen;
}

void SendAPacket(){
	// for 1 client (now for client 1)
	MyPacket packet; // this is the struct description
Send((char *)&packet, sizeof(packet), 1); // our function that we have created to send data
   //for all clients
for (int i = 0; i < maxclients; i++)
{
	MyPacket packet; // this is the struct description
	Send((char *)&packet, sizeof(packet), i); // our function that we have created to send data 
}
}

void RecieveAPacket()
{
	// for 1 client
	MyPacket packet; // this is the struct description
	Recieve((char *)&packet, sizeof(packet), 1); // our function that we have created to recive data 
	// for all clients
	for (int i = 0; maxclients < 4; i++)
	{
		MyPacket packet; // this is the struct description
		Recieve((char *)&packet, sizeof(packet), i); // our function that we have created to recive data 
	}

}


void SendBool(bool boo)
{
	char data_to_send[sizeof(boo)];
	memcpy(&data_to_send, &boo, sizeof(boo));
	send(sock, (const char*)data_to_send, sizeof(boo), 0);
}

bool RecieveBool()
{
	bool boo;
	char buffer[sizeof(boo)];
	int clientid = 1;
	recv(sock2[clientid], buffer, sizeof(boo), 0);
	memcpy(&boo, &buffer, sizeof(boo));
	return boo;
}

void SendInt(int boo)
{
	char data_to_send[sizeof(boo)];
	memcpy(&data_to_send, &boo, sizeof(boo));
	send(sock, (const char*)data_to_send, sizeof(boo), 0);
}

int RecieveInt()
{
	int boo;
	char buffer[sizeof(boo)];
	int clientid = 1;
	recv(sock2[clientid], buffer, sizeof(boo), 0);
	memcpy(&boo, &buffer, sizeof(boo));
	return boo;
}

void SendChar(char boo)
{
	char data_to_send[sizeof(boo)];
	memcpy(&data_to_send, &boo, sizeof(boo));
	send(sock, (const char*)data_to_send, sizeof(boo), 0);
}

char RecieveChar()
{
	char boo;
	char buffer[sizeof(boo)];
	int clientid = 1;
	recv(sock2[clientid], buffer, sizeof(boo), 0);
	memcpy(&boo, &buffer, sizeof(boo));
	return boo;
}

void SendShort(short boo)
{
	char data_to_send[sizeof(boo)];
	memcpy(&data_to_send, &boo, sizeof(boo));
	send(sock, (const char*)data_to_send, sizeof(boo), 0);
}

short RecieveShort()
{
	short boo;
	char buffer[sizeof(boo)];
	int clientid = 1;
	recv(sock2[clientid], buffer, sizeof(boo), 0);
	memcpy(&boo, &buffer, sizeof(boo));
	return boo;
}

void SendLong(long boo)
{
	char data_to_send[sizeof(boo)];
	memcpy(&data_to_send, &boo, sizeof(boo));
	send(sock, (const char*)data_to_send, sizeof(boo), 0);
}

long RecieveLong()
{
	long boo;
	char buffer[sizeof(boo)];
	int clientid = 1;
	recv(sock2[clientid], buffer, sizeof(boo), 0);
	memcpy(&boo, &buffer, sizeof(boo));
	return boo;
}

void SendLongLong(long long boo)
{
	char data_to_send[sizeof(boo)];
	memcpy(&data_to_send, &boo, sizeof(boo));
	send(sock, (const char*)data_to_send, sizeof(boo), 0);
}

long long RecieveLongLong()
{
	long long boo;
	char buffer[sizeof(boo)];
	int clientid = 1;
	recv(sock2[clientid], buffer, sizeof(boo), 0);
	memcpy(&boo, &buffer, sizeof(boo));
	return boo;
}

void SendFloat(float boo)
{
	char data_to_send[sizeof(boo)];
	memcpy(&data_to_send, &boo, sizeof(boo));
	send(sock, (const char*)data_to_send, sizeof(boo), 0);
}

float RecieveFloat()
{
	float boo;
	char buffer[sizeof(boo)];
	int clientid = 1;
	recv(sock2[clientid], buffer, sizeof(boo), 0);
	memcpy(&boo, &buffer, sizeof(boo));
	return boo;
}

void SendDouble(double boo)
{
	char data_to_send[sizeof(boo)];
	memcpy(&data_to_send, &boo, sizeof(boo));
	send(sock, (const char*)data_to_send, sizeof(boo), 0);
}

double RecieveDouble()
{
	double boo;
	char buffer[sizeof(boo)];
	int clientid = 1;
	recv(sock2[clientid], buffer, sizeof(boo), 0);
	memcpy(&boo, &buffer, sizeof(boo));
	return boo;
}

void SendLongDouble(long double boo)
{
	char data_to_send[sizeof(boo)];
	memcpy(&data_to_send, &boo, sizeof(boo));
	send(sock, (const char*)data_to_send, sizeof(boo), 0);
}

long double RecieveLongDouble()
{
	long double boo;
	char buffer[sizeof(boo)];
	int clientid = 1;
	recv(sock2[clientid], buffer, sizeof(boo), 0);
	memcpy(&boo, &buffer, sizeof(boo));
	return boo;
}

void SendString(string boo)
{
	char data_to_send[sizeof(boo)];
	memcpy(&data_to_send, &boo, sizeof(boo));
	send(sock, (const char*)data_to_send, sizeof(boo), 0);
}

string RecieveString()
{
	string boo;
	char buffer[sizeof(boo)];
	int clientid = 1;
	recv(sock2[clientid], buffer, sizeof(boo), 0);
	memcpy(&boo, &buffer, sizeof(boo));
	return boo;
}

int EndSocket()
{
	closesocket(sock);
	WSACleanup();
	cin.get();
	return 1;
}

int Send(SOCKET s, const char * FAR buf, int len, int flags)
{
	int nret;
	char buffer[256];		// Declaring a buffer on the stack
	char *buffer = new char[256];	// or on the heap
	ZeroMemory(buffer, 256);
	strcpy(buffer, "Pretend this is important data.");
	nret = send(sock,
		buffer,
		strlen(buffer),	// Note that this specifies the length of the string; not
						// the size of the entire buffer
		0);			// Most often is zero, but see MSDN for other options
	delete[] buffer;		// If and only if the heap declaration was used
	if (nret == SOCKET_ERROR)
	{

		// Get a specific code

		// Handle accordingly
		cout << "Socket Error" << endl;
		cin.get();
		return 1;
	}
	else {
		// nret contains the number of bytes sent
	}
}

int Recieve(SOCKET s, const char * FAR buf, int len, int flags)
{
	int nret;
	char buffer[256];		// On the stack
	char *buffer = new char[256];	// or on the heap
	nret = recv(sock,
		buffer,
		256,		// Complete size of buffer
		0);
	delete[] buffer;		// Manipulate buffer, then delete if and only if
							// buffer was allocated on heap
	if (nret == SOCKET_ERROR)
	{
		// Get a specific code
		// Handle accordingly
		cout << "Socket Error" << endl;
		cin.get();
		return 1;
	}
	else {
		// nret contains the number of bytes received
	}
}