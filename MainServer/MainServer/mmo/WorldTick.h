#pragma once
#include <chrono>

typedef std::function<void()> Func;

class WorldTick
{

public:
	WorldTick(WorldTick const&) = delete;
	void operator =(WorldTick const&) = delete;

	static WorldTick& Instance();
	template<class Fn, class Ins>
	void AddTimer(Ins instance, Fn function, double seconds, bool lockThread = true);
	template<class Fn>
	void AddTimerStatic(Fn function, double seconds, bool lockThread = true);
	template<class Fn, class Ins>
	void AddNextTick(Ins instance, Fn function);
	template<class Fn>
	void AddNextTickStatic(Fn function);

	void RunTest();
	void TestMethod();
	void StopTimer();

private:
	WorldTick();
	~WorldTick();

	static void TickThreaded();

	int testVar = 1337;
	static void StaticUpdateTest();

	static double ticksPerSecond;// Number of updates per second - set in cpp.
	static bool run; // is set to false when exiting program.
	static const int hiResSeconds = 10; // Number of seconds counted in game ticks.
	static const int maxSeconds = 300; // Max number of seconds used in timer.
	static std::mutex tickMutex;

	static std::vector< std::vector< Func > > tickBuffer;
	static int tickBufferPos;

	static std::vector< std::vector< Func > > secondBuffer;
	static int secondBufferPos;
};

template<class Fn, class Ins> // Use to run functions after some time, 0 is next tick. Works from different threads.
							  //  Use -this- as instance parameter.
							  // Use &ClassName::FunctionName as function parameter.
inline void WorldTick::AddTimer(Ins instance, Fn function, double seconds, bool lockThread)
{
	if (lockThread) std::lock_guard<std::mutex> lock(tickMutex);
	seconds = seconds < 0 ? 0 : seconds;
	if (seconds < hiResSeconds) {
		tickBuffer[((int)round(tickBufferPos + seconds*ticksPerSecond + 1)) % tickBuffer.size()].push_back(std::bind(function, instance));
	}
	else {
		seconds = seconds >= maxSeconds ? maxSeconds : seconds;
		secondBuffer[((int)round(secondBufferPos + seconds - 1)) % secondBuffer.size()].push_back(std::bind(function, instance));
	}
}

template<class Fn> //Same as AddTimer, but for static functions.
inline void WorldTick::AddTimerStatic(Fn function, double seconds, bool lockThread)
{
	if (lockThread) std::lock_guard<std::mutex> lock(tickMutex);
	seconds = seconds < 0 ? 0 : seconds;
	if (seconds < hiResSeconds) {
		tickBuffer[((int)round(tickBufferPos + seconds*ticksPerSecond + 1)) % tickBuffer.size()].push_back(std::bind(function));
	}
	else {
		seconds = seconds >= maxSeconds ? maxSeconds : seconds;
		secondBuffer[((int)round(secondBufferPos + seconds - 1)) % secondBuffer.size()].push_back(std::bind(function));
	}
}

template<class Fn, class Ins>//Faster version for update every tick. USE ONLY FROM FUNCTIONS ALREADY RUNNING FROM WORLDTICK - only the same thread. From other thread use AddTimer with 0 seconds.
inline void WorldTick::AddNextTick(Ins instance, Fn function)
{
	tickBuffer[(tickBufferPos + 1) % tickBuffer.size()].push_back(std::bind(function, instance));
}

template<class Fn>//Faster version for update every tick. USE ONLY FROM FUNCTIONS ALREADY RUNNING FROM WORLDTICK - only the same thread. From other thread use AddTimer with 0 seconds.
inline void WorldTick::AddNextTickStatic(Fn function)
{
	tickBuffer[(tickBufferPos + 1) % tickBuffer.size()].push_back(std::bind(function));
}