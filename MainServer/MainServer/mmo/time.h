#pragma once
#include <chrono>
#include <iostream>

using namespace std;
namespace sc = chrono;

//To get current time of the server machine in seconds
long getCurrentMillis()
{
	auto time = sc::system_clock::now();
	auto since_epoch = time.time_since_epoch();
	auto millis = sc::duration_cast<sc::milliseconds>(since_epoch);
	long now = millis.count();
	return now;
}