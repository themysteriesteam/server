#include "WorldTick.h"
#include <chrono>
#include <iostream>
#include <vector>
using namespace std;
using namespace chrono;

double WorldTick::ticksPerSecond = 30;//How many updates per second
bool WorldTick::run = true;
std::mutex WorldTick::tickMutex;

int WorldTick::tickBufferPos = 0;
std::vector< std::vector< Func > > WorldTick::tickBuffer;

int WorldTick::secondBufferPos = 0;
std::vector< std::vector< Func > > WorldTick::secondBuffer;

WorldTick & WorldTick::Instance() //Makes one instance in the program - singleton
{
	static WorldTick instance;
	return instance;
}


WorldTick::WorldTick()
{
	tickBuffer = std::vector< std::vector< Func > >(hiResSeconds*(unsigned int)ticksPerSecond);
	secondBuffer = std::vector< std::vector< Func > >(maxSeconds);

	CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)TickThreaded, NULL, NULL, NULL);

	//RunTest();  //Test if the timer is working properly
}

auto testTime = high_resolution_clock::now().time_since_epoch();
void WorldTick::RunTest() {
	testTime = high_resolution_clock::now().time_since_epoch();

	//AddNextTickStatic(&WorldTick::StaticUpdateTest);

	AddTimer(this, &WorldTick::TestMethod, 0);
	AddTimer(this, &WorldTick::TestMethod, 0.1);
	AddTimer(this, &WorldTick::TestMethod, 0.5);

	AddTimer(this, &WorldTick::TestMethod, 1);
	AddTimer(this, &WorldTick::TestMethod, 5);
	AddTimer(this, &WorldTick::TestMethod, 7.5);

	AddTimer(this, &WorldTick::TestMethod, 12);
	AddTimer(this, &WorldTick::TestMethod, 26);
	AddTimer(this, &WorldTick::TestMethod, 55);

	AddTimer(this, &WorldTick::TestMethod, 80);
	AddTimer(this, &WorldTick::TestMethod, 125);
	AddTimer(this, &WorldTick::TestMethod, 260);

	AddTimer(this, &WorldTick::TestMethod, 305); // out of bounds
}
void WorldTick::TestMethod()
{
	cout << "Test tick at time: " << (high_resolution_clock::now().time_since_epoch() - testTime).count() / 1000000000.0 << "...member variable: " << testVar << endl;
}



WorldTick::~WorldTick()
{
	run = false;
}

void WorldTick::StopTimer()
{
	run = false;
}

void WorldTick::TickThreaded() //ref: chrono, Hashed and Hierarchical Timing Wheels
{
	auto tickLength = nanoseconds((int)(1000000000.0 / ticksPerSecond));
	double tickLength_d = (1.0 / ticksPerSecond);
	auto startTime = high_resolution_clock::now();
	double seconds = -1.0;
	double oldSeconds = 0.0;
	while (run) {
		if (high_resolution_clock::now() - startTime > tickLength) {
			std::lock_guard<std::mutex> lock(tickMutex);
			//cout << "Update took " << (high_resolution_clock::now() - startTime).count()/ 1000000000.0 << " seconds." << endl; // use for debug
			oldSeconds = seconds;
			seconds += tickLength_d;
			if ((int)oldSeconds != (int)seconds) {
				for each (auto var in secondBuffer[secondBufferPos])
				{
					var();
				}
				secondBuffer[secondBufferPos].clear();
				secondBufferPos = (secondBufferPos + 1) % secondBuffer.size();
			}


			for each (auto var in tickBuffer[tickBufferPos])
			{
				var();
			}
			tickBuffer[tickBufferPos].clear();


			startTime += tickLength;

			tickBufferPos = (tickBufferPos + 1) % tickBuffer.size();
		}
	}
}

void WorldTick::StaticUpdateTest()
{
	std::cout << "Update" << std::endl;
	WorldTick::Instance().AddNextTickStatic(&WorldTick::StaticUpdateTest);

}
