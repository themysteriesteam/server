#pragma once
#include <Windows.h>
#include <winsock.h>
#include <iostream>
#include <string>

using namespace std;

#pragma comment(lib,"ws2_32.lib")

SOCKET sock; // this is the socket we will use
SOCKET sock2[200]; // this is the sockets that will be recived from the Clients and sended to them
SOCKADDR_IN i_sock2; // this will containt informations about the clients connected to the server
SOCKADDR_IN i_sock; // this will containt some informations about our socket
WSADATA Data; // this is to save our socket version
int clients = 0; // we will use it in the accepting clients
int maxclients = 500;

int StartServer(char *IP, int Port);
int Send(char *Buf, int len, int Client);
int Recieve(char *Buf, int len, int Client);
void SendAPacket();
void RecieveAPacket();
int EndSocket();
void SendBool(bool boo); //true or false
bool RecieveBool();
void SendInt(int boo); //int value
int RecieveInt();
void SendChar(char boo);
char RecieveChar();
void SendShort(short boo);
short RecieveShort();
void SendLong(long boo);
long RecieveLong();
void SendLongLong(long long boo);
long long RecieveLongLong();
void SendFloat(float boo);
float RecieveFloat();
void SendDouble(double boo);
double RecieveDouble();
void SendLongDouble(long double boo);
long double RecieveLongDouble();
void SendString(string boo);
string RecieveString();
int Send(SOCKET s, const char * FAR buf, int len, int flags);

struct MyPacket
{
	//Char packet
	int myint;
	char mychar[256];
}; 

void sendmessage(string message)
{
	string _message = message;
	SendString(_message);
}
