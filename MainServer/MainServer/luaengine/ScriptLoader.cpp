#include "ScriptLoader.h"

//macro definition for *safe* deletion of pointer object
#define SAFE_DELETE(x) { if(x) delete x; x = nullptr; }

void ScriptLoad()
{
	std::cout << "Loading Scripts" << std::endl;
	//create lua engine
	LuaEngine* engine = new LuaEngine();
	//execute specified file file
	engine->ExecuteFile("scripts/test.lua");
}

void EngineDelete()
{
//	SAFE_DELETE(engine); //delete engine from memory
	// has error needs to be fixed
	std::cout << "Lua Engine Deleted From The Memory" << std::endl;
}