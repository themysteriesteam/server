#pragma once
#include "../mmo/server.h"
#include "../mmo/WorldTick.h"

using namespace std;

class SingletonHolder
{
public:
 ShutDownServer _instance;
};

class ShutDownServer {
public:
	SingletonHolder s;
	int SIGTERM = 0;
	int GM_SHUTDOWN = 1;
	int GM_RESTART = 2;
	int ABORT = 3;
	void ShutDown(int seconds, bool restart)
	{
		if (seconds < 0)
			seconds = 0;
		_secondsShut = seconds;
		if (restart)
		{
			_shutdownMode = GM_RESTART;
			servermode = CLOSING;
		}
		else
		{
			_shutdownMode = GM_SHUTDOWN;
			servermode = CLOSING;
			EndSocket();
		}
	}
	ShutDownServer()
	{
		_secondsShut = -1;
		_shutdownMode = SIGTERM;
	}
	ShutDownServer getInstance()
	{
		return s._instance;
	}
	void StartShutDown(int seconds, bool restart)
	{
		if (_shutdownMode > 0)
		{
			switch (seconds)
			{
			case 540:
			case 480:
			case 420:
			case 360:
			case 300:
			case 240:
			case 180:
			case 120:
			case 60:
			case 30:
			case 10:
			case 5:
			case 4:
			case 3:
			case 2:
			case 1:
				break;
			default:
				SendServerQuit(seconds);
				worldtick.AddTimer(this, &ShutDownServer::ShutDown, seconds);
				//ShutDown = true;
			}
		}
	}
	void Abort()
	{
		_shutdownMode = 0;
		_secondsShut = 0;
	}
	void Run()
	{
		if (ShutDown)
		{
			w.StopTimer();
			EndSocket();
				//save etc
		}
	}
private:
	bool ShutDown = false;
	string MODE_TEXT[4] = { "SIGTERM","Shutting Down","Restarting","Aborting" };
	int _secondsShut;
	int _shutdownMode;
	void SendServerQuit(int seconds)
	{
		sendmessage("Seconds to shutdown:" + seconds);
	}
};