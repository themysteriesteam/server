/**-------------------------------------------------------------------
*  Include database-detail.h and other files
*------------------------------------------------------------------*/

#include "mysql\my_global.h"
#include "mysql\mysql.h"
#include "connector\mysql_connection.h"
#include <iostream>
#include "database-detail.h"

#pragma comment(lib, "libmysql.lib")

using namespace std;

/**
* ===================================================================
*        Class:  MySQL
*  Description:  MySQL class for database accessability
* ===================================================================
*/

class MySQL
{
protected:
	/** MySQL connectivity Variables */
	MYSQL *connect;
	MYSQL_RES *res_set;
	MYSQL_ROW row;

	unsigned int i;

public:
	/** MySQL Constructor */
	MySQL();

	/** Function to show tables in database */
	void ShowTables();

	/** MySQL Destructor */
	~MySQL();
};